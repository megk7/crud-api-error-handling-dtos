import { EntityRepository, Repository } from "typeorm";
import { Task } from "./task.entity";
import { TaskStatus } from "./task.status.enum";

@EntityRepository(Task)
export class TaskRepository extends Repository<Task>{
    async createTask(createTaskDto): Promise<Task>{
        const {title,description} = createTaskDto;
        const task = this.create({
          title,
          description,
          status:TaskStatus.OPEN,
        });
        
        await this.save(task);
        return task;
      }
}