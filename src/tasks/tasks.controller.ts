import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { TasksService } from './tasks.service';
import { Body, Controller, Delete, Get, Param, Patch, Post, Query } from '@nestjs/common';
import { TaskStatus } from './task.status.enum';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskStatusDto } from './dto/update-task-status.dto';
import { Task } from './task.entity';

@Controller('tasks')
export class TasksController {
  constructor(private tasksservice: TasksService) {}
  // @Get()
  // getTasks(@Query() filterDto:GetTasksFilterDto): Task[] {
  //   if(Object.keys(filterDto).length){
  //     return this.tasksservice.getTasksWithFilter(filterDto);
  //   }
  //   else{
  //     return this.tasksservice.getAllTasks();
  //   }
  // }

  @Get('/:id')
  getTaskById(@Param('id') id:string): Promise<Task>{
    return this.tasksservice.getTaskById(id);
  }

  // @Get('/:id')
  // getTaskById(@Param('id') id:string): Task{
  //   return this.tasksservice.getTaskById(id);
  // }

  @Post()
  createTask(
    @Body() createTaskDto:CreateTaskDto
  ): Promise<Task> {
    return this.tasksservice.createTask(createTaskDto);
  }

  @Delete('/:id')
  deleteTask(@Param('id') id:string): Promise<void>{
    return this.tasksservice.deleteTask(id);
  }

  @Patch('/:id/status')
  updateTaskStatus(
    @Param('id') id:string,
    @Body() updateTaskStatusDto:UpdateTaskStatusDto,
  ): Promise<Task>{
    const {status} = updateTaskStatusDto;
    return this.tasksservice.updateTaskStatus(id,status);
  }
}
